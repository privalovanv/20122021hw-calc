package app;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int viborOperac;
        System.out.println("Здравствуй, Олег! Метод <<Гугл в помощь>> навел меня на такую внезапную реализацию  калькулятора. Итак, что будем выполнять: 1-сложение, 2 вычитание, 3 - умножение, 4- деление. Напиши свой выбор");
        viborOperac=in.nextInt();
        do{
            if(viborOperac>7){
                System.out.print("Неверно ввел.Итак, что будем выполнять: 1-сложение, 2 вычитание, 3 - умножение, 4- деление.");
                viborOperac=in.nextInt();
            }
        } while (viborOperac>7);
        switch(viborOperac) {

            case 1:
                System.out.println("Ты выбрал сложение");
                System.out.print("Первое слагаемое = ");
                double slagaemoe1 = in.nextDouble();
                System.out.print("Второе слагаемое = ");
                double slagaemoe2 = in.nextDouble();
                System.out.printf("Сумма = %.3f \n", slagaemoe1 + slagaemoe2);
                break;

            case 2:
                System.out.println("Ты выбрал вычитание");
                System.out.print("Уменьшаемое = ");
                double umenshaemoe1 = in.nextDouble();
                System.out.print("Вычитаемое = ");
                double vichitaemoe2 = in.nextDouble();
                System.out.printf("Разность = %.3f \n", umenshaemoe1 - vichitaemoe2);
                break;

            case 3:
                System.out.println("Ты выбрал умножение");
                System.out.print("Первый множитель = ");
                double mnozhitel1 = in.nextDouble();
                System.out.print("Второй множитель = ");
                double mnozhitel2 = in.nextDouble();
                System.out.printf("Произведение = %.3f \n", mnozhitel1 * mnozhitel2);
                break;

            case 4:
                double delimoe;
                double delitel;
                System.out.println("Ты выбрал деление");
                System.out.print("Делимое = ");
                delimoe = in.nextDouble();
                System.out.print("Делитель = ");
                delitel = in.nextDouble();
                double chastnoe = delimoe / delitel;
                System.out.printf("Частное = %.3f \n", chastnoe);
                break;

        }
    }
}


